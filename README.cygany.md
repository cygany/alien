It is a fork of the alien package, which can convert installation packages
between various formats. There was a plan to use it also in the project
Cygany, to convert cygwin packages to deb. But ultimately I decided to use
a shellscript for that. This repo now remains as it is.

It is a fork of the debian alien upstream from the salsa.debian.org,
currently a git remote -v results this:

origin	git@gitlab.com:cygany/alien.git (fetch)
origin	git@gitlab.com:cygany/alien.git (push)
upstream	https://salsa.debian.org/debian/alien.git (fetch)
upstream	https://salsa.debian.org/debian/alien.git (push)

Our master branch (in origin/master) is our fork of the debian
master branch (remotes/upstream/master).

In the current state of the Cygany development, I compiled this package
with the command

PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules binary
